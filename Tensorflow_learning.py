import numpy as np
import tensorflow as tf


def get_train_data(path_to_data=""):
    X, Y = np.empty([30100, 4]), np.empty(30100)
    for i in range(0, 30100):
        with open(path_to_data + "norm_x/" + str(i) + ".txt", 'r') as f:
            x = [float(line.strip()) for line in f if line]
        with open(path_to_data + "norm_y/" + str(i) + ".txt", 'r') as f:
            y = f.readline()
        for k in range(len(x)):
            X[i][k] = float("{:.5f}".format(float(x[k])))
        Y[i] = float("{:.6f}".format(float(y)))
    return X, Y


def get_model1():
    model = tf.keras.models.Sequential()
    model.add(tf.keras.layers.InputLayer(input_shape=(4,)))
    model.add(tf.keras.layers.Dense(50, activation='relu', kernel_initializer='random_normal'))
    model.add(tf.keras.layers.Dense(1, activation='linear'))
    model.compile(optimizer=tf.keras.optimizers.RMSprop(), metrics=['accuracy'], loss='mean_squared_error')
    return model


def get_model2():
    model = tf.keras.models.Sequential()
    model.add(tf.keras.layers.Dense(50, input_dim=4, kernel_initializer='uniform', activation='LeakyReLU'))
    model.add(tf.keras.layers.Dense(20, activation='LeakyReLU'))
    model.add(tf.keras.layers.Dense(1, activation='LeakyReLU'))
    model.compile(loss='mean_squared_error', optimizer='adam', metrics=['accuracy'])
    return model


def get_model3():
    model = tf.keras.models.Sequential()
    model.add(tf.keras.layers.Dense(100, input_dim=4, kernel_initializer='uniform', activation='relu'))
    model.add(tf.keras.layers.Dense(50, activation='relu'))
    model.add(tf.keras.layers.Dense(1, activation='linear'))
    model.compile(loss='mean_squared_error', optimizer='adam', metrics=['accuracy'])
    return model


def get_model4():
    model = tf.keras.models.Sequential()
    model.add(tf.keras.layers.Dense(50, input_dim=4, kernel_initializer='uniform', activation='LeakyReLU'))
    model.add(tf.keras.layers.Dense(20, activation='LeakyReLU'))
    model.add(tf.keras.layers.Dense(20, activation='LeakyReLU'))
    model.add(tf.keras.layers.Dense(1, activation='LeakyReLU'))
    model.compile(loss='mean_squared_error', optimizer='adam', metrics=['accuracy'])
    return model


def get_model5():
    model = tf.keras.models.Sequential()
    model.add(tf.keras.layers.Dense(700, input_dim=4, kernel_initializer='RandomUniform', activation='LeakyReLU'))
    model.add(tf.keras.layers.Dense(500, kernel_initializer='RandomUniform', activation='LeakyReLU'))
    model.add(tf.keras.layers.Dense(300, kernel_initializer='RandomUniform', activation='LeakyReLU'))
    model.add(tf.keras.layers.Dense(200, kernel_initializer='RandomUniform', activation='LeakyReLU'))
    model.add(tf.keras.layers.Dense(1, kernel_initializer='RandomUniform', activation='tanh'))
    model.compile(loss='mean_squared_error', optimizer='adam', metrics=['accuracy'])
    return model


def learn_model(model, X, Y):
    reduce_lr = tf.keras.callbacks.ReduceLROnPlateau(monitor='val_loss', factor=0.9, patience=5, min_lr=0.000001)
    model.fit(X, Y, epochs=1000, validation_split=0.1, batch_size=2048, shuffle=True)
    model.fit(X, Y, epochs=500, validation_split=0.1, callbacks=[reduce_lr], batch_size=2048, shuffle=True)
    model.save("learned_model.h5")


def test_model(model, X, Y):
    res = model.predict(X)
    maxx = 15.099
    minn = -15.0
    mae = 0
    maxe = 0
    mrpe = 0
    for i in range(len(res)):
        true = round(Y[i] * (maxx - minn) + minn, 2)
        predicted = round(res[i][0] * (maxx - minn) + minn, 2)
        mae += np.absolute(predicted - true)
        if true != 0:
            mrpe += (np.absolute(predicted - true) / np.absolute(true)) * 100
        if np.absolute(predicted - true) > maxe:
            maxe = np.absolute(predicted - true)
    mae = mae / len(res)
    mrpe = mrpe / len(res)
    print("Mean avarage error (°C): ", mae)
    print("Mean relation error (%): ", mrpe)
    print("Max absulute error (°C): ", maxe)


if __name__ == "__main__":
    X,Y = get_train_data()
    nn_model = get_model5()
    learn_model(nn_model, X, Y)
    test_model(nn_model, X, Y)


