# FPI Temperature model

## Dependencies

- [ ] [Numpy (1.24.3)](https://pypi.org/project/numpy/) 
- [ ] [Tensorflow (2.13.0rc1)](https://pypi.org/project/tensorflow/)
- [ ] Python (3.11.3)
 
___
## Train data

- [ ] [Download](https://drive.google.com/file/d/1_U3jYBVPBhOe4ovFIzSHhIjPVofEesc0/view?usp=sharing) train data;
- [ ] Unpack archive;
- [ ] Pass path to train data to `get_train_data` function;
- [ ] Run script to learn model;
___
#### Archive with normalized data to train models - https://drive.google.com/file/d/1_U3jYBVPBhOe4ovFIzSHhIjPVofEesc0/view?usp=sharing